package api.uniwizard.responses;

public class GetEntityResponse<T> implements IndexResponse<T> {
    T result;

    public GetEntityResponse() {

    }

    public GetEntityResponse(T result) {
        this.result = result;
    }

    @Override
    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }
}
