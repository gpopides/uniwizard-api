package api.uniwizard.responses;

public interface IndexResponse<T> {
    T getResult();
}
