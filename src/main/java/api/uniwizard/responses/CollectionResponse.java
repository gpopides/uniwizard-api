package api.uniwizard.responses;

public interface CollectionResponse<T> {
    T getItems();
}
