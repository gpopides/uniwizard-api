package api.uniwizard.responses;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class GetCollectionResponse<T> implements CollectionResponse<T> {
    T collection;

    @Override
    public T getItems() {
        return collection;
    }
}
