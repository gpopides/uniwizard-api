package api.uniwizard.util;

public enum SubjectType {
    SOFTWARE,
    HARDWARE,
    ECONOMICS,
    PHYSICS,
    MATH,
    STATISTICS,
    ALGORITHMS,
    DATA_STRUCTURES
}
