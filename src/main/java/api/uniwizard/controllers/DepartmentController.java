package api.uniwizard.controllers;

import api.uniwizard.dto.CurriculumDto;
import api.uniwizard.dto.UniversityDepartmentInfoDto;
import api.uniwizard.responses.GetEntityResponse;
import api.uniwizard.responses.IndexResponse;
import api.uniwizard.services.UniversityDepartmentService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class DepartmentController {
    private final UniversityDepartmentService universityDepartmentService;

    public DepartmentController(UniversityDepartmentService universityDepartmentService) {
        this.universityDepartmentService = universityDepartmentService;
    }

    @GetMapping("departments/{departmentId}/curricula")
    public IndexResponse<List<CurriculumDto>> getCurricula(@PathVariable int departmentId) {
        return new GetEntityResponse<>(universityDepartmentService.getCurriculaOfDepartment(departmentId));
    }

    @GetMapping("departments/{departmentId}/info")
    public IndexResponse<UniversityDepartmentInfoDto> getDepartmentInfo(
            @PathVariable int departmentId,
            @PathParam("curriculumId") int curriculumId
    ) {
        UniversityDepartmentInfoDto info = universityDepartmentService
                .getDepartmentInfo(departmentId, curriculumId)
                .getData();
        return new GetEntityResponse<>(info);
    }

}


