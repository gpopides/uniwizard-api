package api.uniwizard.controllers;

import api.uniwizard.dto.SubjectDto;
import api.uniwizard.entities.Subject;
import api.uniwizard.responses.GetEntityResponse;
import api.uniwizard.responses.IndexResponse;
import api.uniwizard.responses.UpdateEntityResponse;
import api.uniwizard.serviceresults.UpdateEntityResult;
import api.uniwizard.services.SubjectService;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;

@RestController
@CrossOrigin("http://localhost:3000")
public class SubjectController {
    private final SubjectService subjectService;

    public SubjectController(SubjectService subjectService) {
        this.subjectService = subjectService;
    }

    @PostMapping("subjects/edit/{subjectId}")
    public UpdateEntityResponse<Subject> editSubject(@RequestBody SubjectDto subjectDto, @PathVariable int subjectId) {
        UpdateEntityResult<Subject> result = subjectService.updateSubject(subjectId, subjectDto);
        return new UpdateEntityResponse<>(result.getUpdatedEntity());
    }

    @GetMapping("subjects")
    public IndexResponse<Iterable<Subject>> getSubjects(@PathParam("curriculumId") int curriculumId) {
        return new GetEntityResponse<>(subjectService.getCurriculumSubjects(curriculumId));
    }

}
