package api.uniwizard.controllers;

import api.uniwizard.dto.UniversityDto;
import api.uniwizard.responses.GetCollectionResponse;
import api.uniwizard.responses.CollectionResponse;
import api.uniwizard.responses.GetEntityResponse;
import api.uniwizard.responses.IndexResponse;
import api.uniwizard.services.UniversityService;
import api.uniwizard.util.DepartmentType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;
import java.util.List;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class UniversityController {
    private final UniversityService universityService;

    public UniversityController(UniversityService universityService) {
        this.universityService = universityService;
    }


    @GetMapping("universities")
    public CollectionResponse<List<UniversityDto>> getUniversities(@PathParam("type") DepartmentType type) {
        return new GetCollectionResponse<>(universityService.getUniversities(type));
    }

    @GetMapping("universities/{universityId}")
    public IndexResponse<UniversityDto> getUniversity(@PathVariable int universityId) {
        GetEntityResponse<UniversityDto> response = new GetEntityResponse<>();

        universityService.getUniversity(universityId)
        .ifPresentOrElse((university) -> response.setResult(UniversityDto.of(university)), () -> System.out.println("what"));

        return response;
    }

}
