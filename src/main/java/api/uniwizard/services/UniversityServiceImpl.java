package api.uniwizard.services;

import api.uniwizard.dto.UniversityDto;
import api.uniwizard.entities.City;
import api.uniwizard.entities.University;
import api.uniwizard.repositories.UniversityRepository;
import api.uniwizard.util.DepartmentType;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UniversityServiceImpl implements UniversityService {
    private final UniversityRepository universityRepository;

    public UniversityServiceImpl(UniversityRepository universityRepository) {
        this.universityRepository = universityRepository;
    }

    @Override
    public List<UniversityDto> getUniversities(DepartmentType type) {
        List<UniversityDto> universityDtos = new ArrayList<>();
        if (type == null) {
            var x = universityRepository.findAll();
            universityRepository
                    .findAll()
                    .iterator()
                    .forEachRemaining(university -> universityDtos.add(UniversityDto.of(university)));
        } else {
            var x = universityRepository.findByDepartmentsType(type);
            universityRepository
                    .findByDepartmentsType(type)
                    .iterator()
                    .forEachRemaining(university -> universityDtos.add(UniversityDto.of(university)));
        }
        universityDtos.sort(Comparator.comparing(UniversityDto::getName));
        return universityDtos;
    }

    @Override
    public Optional<City> getCityOfUniversity(int universityId) {
        Optional<University> university = universityRepository.findById(universityId);
        return university.map(University::getCity);
    }


    @Override
    public Iterable<University> getUniversitiesOfCity(int cityId) {
        return universityRepository.findByCityIdOrderByName(cityId);
    }


    @Override
    public Optional<University> getUniversity(int universityId) {
        return universityRepository.findById(universityId);
    }
}
