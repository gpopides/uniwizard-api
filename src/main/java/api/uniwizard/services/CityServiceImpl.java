package api.uniwizard.services;

import api.uniwizard.entities.City;
import api.uniwizard.repositories.CityRepository;
import org.springframework.stereotype.Service;

@Service
public class CityServiceImpl implements CityService {
    private final CityRepository cityRepository;

    public CityServiceImpl(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }

    @Override
    public Iterable<City> cities() {
        return cityRepository.findAll();
    }
}
