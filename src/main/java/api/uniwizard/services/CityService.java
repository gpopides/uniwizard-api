package api.uniwizard.services;

import api.uniwizard.entities.City;

public interface CityService {
    Iterable<City> cities();
}
