package api.uniwizard.services;

import api.uniwizard.dto.CurriculumDto;
import api.uniwizard.dto.UniversityDepartmentInfoDto;
import api.uniwizard.entities.Subject;
import api.uniwizard.entities.UniversityDepartment;
import api.uniwizard.serviceresults.ServiceResult;

import java.util.List;
import java.util.Map;

public interface UniversityDepartmentService {
    Iterable<UniversityDepartment> getDepartmentsOfUniversity(int universityId);
    Iterable<UniversityDepartment> getDepartments();
    Map<Integer, List<Subject>> getGroupedSubjectsOfDepartment(int departmentId, int curriculmnId);
    ServiceResult<UniversityDepartmentInfoDto> getDepartmentInfo(int departmentId, int curriculumId);
    List<CurriculumDto> getCurriculaOfDepartment(int departmentId);
}
