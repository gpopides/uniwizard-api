package api.uniwizard.services;

import api.uniwizard.dto.SubjectDto;
import api.uniwizard.entities.Subject;
import api.uniwizard.serviceresults.UpdateEntityResult;

public interface SubjectService {
    UpdateEntityResult<Subject> updateSubject(int subjectId, SubjectDto subjectDto);
    Iterable<Subject> getCurriculumSubjects(int curriculumId);
}
