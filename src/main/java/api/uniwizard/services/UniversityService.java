package api.uniwizard.services;

import api.uniwizard.dto.UniversityDto;
import api.uniwizard.entities.City;
import api.uniwizard.entities.University;
import api.uniwizard.util.DepartmentType;

import java.util.List;
import java.util.Optional;

public interface UniversityService {
    List<UniversityDto> getUniversities(DepartmentType type);
    Optional<City> getCityOfUniversity(int universityId);
    Iterable<University> getUniversitiesOfCity(int cityId);
    Optional<University> getUniversity(int universityId);
}
