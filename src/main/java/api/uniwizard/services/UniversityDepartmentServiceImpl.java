package api.uniwizard.services;

import api.uniwizard.dto.CurriculumDto;
import api.uniwizard.dto.UniversityDepartmentInfoDto;
import api.uniwizard.entities.Subject;
import api.uniwizard.entities.UniversityDepartment;
import api.uniwizard.repositories.UniversityDepartmentRepository;
import api.uniwizard.serviceresults.ServiceResult;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class UniversityDepartmentServiceImpl implements UniversityDepartmentService {
    private final UniversityDepartmentRepository universityDepartmentRepository;
    private final SubjectService subjectService;

    public UniversityDepartmentServiceImpl(UniversityDepartmentRepository universityDepartmentRepository, SubjectService subjectService) {
        this.universityDepartmentRepository = universityDepartmentRepository;
        this.subjectService = subjectService;
    }

    @Override
    public Iterable<UniversityDepartment> getDepartmentsOfUniversity(int universityId) {
        return universityDepartmentRepository.findAllByUniversityId(universityId);
    }

    @Override
    public Iterable<UniversityDepartment> getDepartments() {
        return universityDepartmentRepository.findAll();
    }

    @Override
    public Map<Integer, List<Subject>> getGroupedSubjectsOfDepartment(int departmentId, int curriculumId) {
        Optional<UniversityDepartment> department = universityDepartmentRepository.findById(departmentId);
        List<Subject> subjects = new ArrayList<>();

        department.flatMap(dep -> dep.getCurricula()
                .stream()
                .filter(c -> c.getId() == curriculumId)
                .findFirst())
                .ifPresent(curriculum -> subjects.addAll(curriculum.getSubjects()));

        return groupSubjectsBySemester(subjects);
    }


    private Map<Integer, List<Subject>> groupSubjectsBySemester(Iterable<Subject> subjects) {
        return StreamSupport
                .stream(subjects.spliterator(), false)
                .collect(Collectors.groupingBy(Subject::getSemester));
    }

    @Override
    public ServiceResult<UniversityDepartmentInfoDto> getDepartmentInfo(int departmentId, int curriculumId) {
        ServiceResult<UniversityDepartmentInfoDto> result = new ServiceResult<>();

        universityDepartmentRepository.findById(departmentId).ifPresentOrElse(
                department -> {
                    UniversityDepartmentInfoDto info = new UniversityDepartmentInfoDto
                            .Builder(department.getNameGr())
                            .setSubjects(subjectService.getCurriculumSubjects(curriculumId))
                            .build();

                    result.setData(info);
                },
                () -> {}
        );

        return result;
    }

    @Override
    public List<CurriculumDto> getCurriculaOfDepartment(int departmentId) {
        List<CurriculumDto> curriculumDtos = new ArrayList<>();

        universityDepartmentRepository
                .findById(departmentId)
                .ifPresent(c -> {
                    List<CurriculumDto> curs = c.getCurricula()
                            .stream()
                            .map(CurriculumDto::of)
                            .collect(Collectors.toList());

                    curriculumDtos.addAll(curs);
                });

        return curriculumDtos;
    }
}
