package api.uniwizard.services;

import api.uniwizard.dto.SubjectDto;
import api.uniwizard.entities.Curriculum;
import api.uniwizard.entities.Subject;
import api.uniwizard.repositories.CurriculumRepository;
import api.uniwizard.repositories.SubjectRepository;
import api.uniwizard.serviceresults.UpdateEntityResult;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class SubjectServiceImpl implements SubjectService {
    private final SubjectRepository subjectRepository;
    private final CurriculumRepository curriculumRepository;

    public SubjectServiceImpl(SubjectRepository subjectRepository, CurriculumRepository curriculumRepository) {
        this.subjectRepository = subjectRepository;
        this.curriculumRepository = curriculumRepository;
    }

    @Override
    public UpdateEntityResult<Subject> updateSubject(int subjectId, SubjectDto subjectDto) {
        UpdateEntityResult<Subject> result = new UpdateEntityResult<>();
        try {
             subjectRepository.findById(subjectId).ifPresent(subject -> {
                 subject.updateAttributesFromDto(subjectDto);

                 if (subjectDto.newCurriculumnExists()) {
                     curriculumRepository
                             .findById(subjectDto.getCurriculumId())
                             .ifPresent(subject::setCurriculum);
                 }

                 subjectRepository.save(subject);
                 result.setUpdatedEntity(subject);
             });
        }
        catch (EntityNotFoundException e) {
            System.out.println(e.getMessage());
        }
        return result;
    }

    @Override
    public Iterable<Subject> getCurriculumSubjects(int curriculumId) {
        return curriculumRepository.findById(curriculumId)
            .map(curriculum -> {
                List<Subject> subs = new ArrayList<>(curriculum.getSubjects());
                subs.sort(Comparator.comparing(Subject::getName));
                return subs;
            })
            .orElse(Collections.emptyList());
    }


}
