package api.uniwizard.graphqlresolvers;

import api.uniwizard.entities.UniversityDepartment;
import api.uniwizard.services.UniversityDepartmentService;
import api.uniwizard.services.UniversityService;
import com.coxautodev.graphql.tools.GraphQLQueryResolver;

public class Query implements GraphQLQueryResolver {
    private final UniversityService universityService;
    private final UniversityDepartmentService universityDepartmentService;

    public Query(UniversityService universityService, UniversityDepartmentService universityDepartmentService) {
        this.universityService = universityService;
        this.universityDepartmentService = universityDepartmentService;
    }

    public Iterable<UniversityDepartment> departments() {
        return universityDepartmentService.getDepartments();
    }
}
