package api.uniwizard.graphqlresolvers;

import api.uniwizard.entities.City;
import api.uniwizard.entities.University;
import api.uniwizard.entities.UniversityDepartment;
import api.uniwizard.services.UniversityDepartmentService;
import api.uniwizard.services.UniversityService;
import com.coxautodev.graphql.tools.GraphQLResolver;

import java.util.ArrayList;
import java.util.List;

public class UniversityResolver implements GraphQLResolver<University> {
    private final UniversityService universityService;
    private final UniversityDepartmentService universityDepartmentService;

    public UniversityResolver(UniversityService universityService, UniversityDepartmentService universityDepartmentService) {
        this.universityService = universityService;
        this.universityDepartmentService = universityDepartmentService;
    }


    public City getCity(University university) {
        return universityService.getCityOfUniversity(university.getId()).orElse(null);
    }

    public Iterable<UniversityDepartment> getDepartments(University university) {
        return universityDepartmentService.getDepartmentsOfUniversity(university.getId());
    }
}
