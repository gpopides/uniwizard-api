package api.uniwizard.graphqlresolvers;

import api.uniwizard.services.CityService;
import api.uniwizard.services.UniversityDepartmentService;
import api.uniwizard.services.UniversityService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ResolversConfig {
    @Bean
    public UniversityResolver universityResolver(UniversityService universityService, UniversityDepartmentService universityDepartmentService) {
        return new UniversityResolver(universityService, universityDepartmentService);
    }


    @Bean
    public Query query(UniversityService universityService, UniversityDepartmentService universityDepartmentService) {
        return new Query(universityService, universityDepartmentService);
    }
}
