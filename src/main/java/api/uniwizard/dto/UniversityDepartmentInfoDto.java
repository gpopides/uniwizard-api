package api.uniwizard.dto;

import api.uniwizard.entities.Subject;

public class UniversityDepartmentInfoDto {

    private final String departmentName;
    private final String description;
    private final Iterable<Subject> subjects;

    private UniversityDepartmentInfoDto(Builder builder) {
        this.departmentName = builder.departmentName;
        this.subjects = builder.subjects;
        this.description = builder.description;
    }

    public static class Builder {
        private final String departmentName;
        private String description;
        private Iterable<Subject> subjects;

        public Builder(String departmentName) {
            this.departmentName = departmentName;
        }

        public Builder setSubjects(Iterable<Subject> subjects) {
            this.subjects = subjects;
            return this;
        }

        public UniversityDepartmentInfoDto build() {
            return new UniversityDepartmentInfoDto(this);
        }

    }

    public String getDepartmentName() {
        return departmentName;
    }

    public Iterable<Subject> getSubjects() {
        return subjects;
    }

    public String getDescription() {
        return description;
    }
}
