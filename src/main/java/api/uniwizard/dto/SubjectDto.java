package api.uniwizard.dto;

import api.uniwizard.entities.Subject;
import api.uniwizard.util.SubjectType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

@Getter
public class SubjectDto {
    private String name;
    private int semester;
    private int id;
    private int curriculumId;
    private SubjectType type;

    private SubjectDto(Subject subject) {
        this.id = subject.getId();
        this.semester = subject.getSemester();
        this.semester = subject.getSemester();
        this.name = subject.getName();
    }

    public boolean newCurriculumnExists() {
        return this.curriculumId > 0;
    }


    public static SubjectDto of(Subject subject) {
        return new SubjectDto(subject);
    }

    @JsonIgnore
    public SubjectDto getDto() {
        return this;
    }
}
