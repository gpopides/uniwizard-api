package api.uniwizard.dto;

import api.uniwizard.entities.Curriculum;

public class CurriculumDto {
    private final String name;
    private final int id;

    public CurriculumDto(Curriculum curriculum) {
        this.name = curriculum.getName();
        this.id = curriculum.getId();
    }

    public static CurriculumDto of(Curriculum curriculum) {
        return new CurriculumDto(curriculum);
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

}
