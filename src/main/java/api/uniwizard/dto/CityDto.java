package api.uniwizard.dto;

import api.uniwizard.entities.City;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CityDto {
    private int id;
    private String name;

    public CityDto(City city) {
        this.id = city.getId();
        this.name = city.getNameGr();
    }

    public static CityDto of(City city) {
        return new CityDto(city);
    }
}
