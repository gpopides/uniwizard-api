package api.uniwizard.dto;

import api.uniwizard.entities.UniversityDepartment;
import lombok.Getter;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Getter
public class UniversityDepartmentDto {
    private final int id;
    private final String name;
    private List<SubjectDto> subjects;

    private UniversityDepartmentDto(UniversityDepartment universityDepartment) {
        this.name = universityDepartment.getNameGr();
        this.id = universityDepartment.getId();
//        setSubjectsOfDepartment(universityDepartment);
    }

    private void setSubjectsOfDepartment(UniversityDepartment universityDepartment) {
        universityDepartment.getCurricula().stream().findFirst().ifPresentOrElse(
                curriculum -> this.subjects = curriculum.getSubjects()
                        .stream()
                        .map(SubjectDto::of)
                        .collect(Collectors.toList()),
                () -> this.subjects = Collections.emptyList()
        );
    }

    public static UniversityDepartmentDto of(UniversityDepartment universityDepartment) {
        return new UniversityDepartmentDto(universityDepartment);
    }
}

