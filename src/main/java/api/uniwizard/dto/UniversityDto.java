package api.uniwizard.dto;

import api.uniwizard.entities.City;
import api.uniwizard.entities.University;
import api.uniwizard.entities.UniversityDepartment;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
public class UniversityDto {
    private String name;
    private int id;
    private CityDto city;
    private List<UniversityDepartmentDto> departments;

    private UniversityDto(University university) {
        this.name = university.getName();
        this.id = university.getId();
        this.city = CityDto.of(university.getCity());
        this.departments = this.createUniversityDepartmentDtos(university.getDepartments());
    }

    public static UniversityDto of(University university) {
        return new UniversityDto(university);
    }

    private List<UniversityDepartmentDto> createUniversityDepartmentDtos(Set<UniversityDepartment> universityDepartmentSet) {
        return new ArrayList<>(universityDepartmentSet)
                .stream()
                .map(UniversityDepartmentDto::of)
                .collect(Collectors.toList());
    }
}
