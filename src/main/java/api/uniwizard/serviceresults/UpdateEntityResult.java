package api.uniwizard.serviceresults;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UpdateEntityResult<T> {
    T updatedEntity;
}
