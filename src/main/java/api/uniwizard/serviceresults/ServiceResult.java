package api.uniwizard.serviceresults;

public class ServiceResult<T> {
    T data;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }



}
