package api.uniwizard.errors;

import lombok.Getter;

@Getter
public class ResponseError {
    private String errorMessage;
}
