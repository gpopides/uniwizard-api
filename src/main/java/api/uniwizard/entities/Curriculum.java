package api.uniwizard.entities;

import lombok.Getter;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "curriculum")
@Getter
public class Curriculum {
    @Id
    private int id;

    @Column(name = "name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "department_id", referencedColumnName = "id")
    private UniversityDepartment universityDepartment;

    @OneToMany(
            fetch = FetchType.EAGER,
            mappedBy = "curriculum",
            cascade = CascadeType.ALL
    )
    private Set<Subject> subjects;

    @Column(name = "is_current")
    private boolean isCurrent;

    @Basic
    @Temporal(TemporalType.DATE)
    @Column(name = "start_date")
    private Date startDate;
}
