package api.uniwizard.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "universities")
@Getter
@Setter
public class University {
    @Id
    private int id;

    @Column(name = "name")
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "city_id", referencedColumnName = "id")
    @JsonIgnore
    private City city;

    @OneToMany(
            mappedBy = "university",
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY
    )
    private Set<UniversityDepartment> departments;
}
