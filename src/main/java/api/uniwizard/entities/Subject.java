package api.uniwizard.entities;

import api.uniwizard.dto.SubjectDto;
import api.uniwizard.util.SubjectType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Table(name = "subjects")
@Getter
@Entity
@Setter
public class Subject {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "semester")
    private int semester;

    @ManyToOne
    @JoinColumn(name = "curriculum_id", referencedColumnName = "id")
    @JsonIgnore
    private Curriculum curriculum;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private SubjectType type;


    public void updateAttributesFromDto(SubjectDto subjectDto) {
        this.name = subjectDto.getName() != null ? subjectDto.getName() : this.name;
        this.semester = subjectDto.getSemester() > 0 ? subjectDto.getSemester() : this.semester;
        this.type = subjectDto.getType() != null ? subjectDto.getType() : this.type;
    }
}
