package api.uniwizard.entities;

import api.uniwizard.util.DepartmentType;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AccessLevel;
import lombok.Getter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "university_departments")
@Getter
public class UniversityDepartment {
    @Id
    private int id;

    @Column(name = "name_en")
    private String nameEn;

    @Column(name = "name_gr")
    private String nameGr;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "university_id", referencedColumnName = "id")
    private University university;

    @OneToMany(
            mappedBy = "universityDepartment",
            cascade = CascadeType.ALL
    )
    private Set<Curriculum> curricula;

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private DepartmentType type;
}
