package api.uniwizard.repositories;

import api.uniwizard.entities.University;
import api.uniwizard.entities.UniversityDepartment;
import api.uniwizard.util.DepartmentType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface UniversityRepository extends CrudRepository<University, Integer> {
    Iterable<University> findByCityIdOrderByName(int cityId);

    @Query("select distinct u from University u inner join u.departments d where d.type = ?1")
    Iterable<University> findByDepartmentsType(DepartmentType type);
}
