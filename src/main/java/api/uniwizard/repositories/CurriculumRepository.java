package api.uniwizard.repositories;

import api.uniwizard.entities.Curriculum;
import org.springframework.data.repository.CrudRepository;

public interface CurriculumRepository extends CrudRepository<Curriculum, Integer> {
}
