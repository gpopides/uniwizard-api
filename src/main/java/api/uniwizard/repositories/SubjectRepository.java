package api.uniwizard.repositories;

import api.uniwizard.entities.Subject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface SubjectRepository extends CrudRepository<Subject, Integer>, JpaRepository<Subject, Integer> {
}
