package api.uniwizard.repositories;

import api.uniwizard.entities.UniversityDepartment;
import org.springframework.data.repository.CrudRepository;

public interface UniversityDepartmentRepository extends CrudRepository<UniversityDepartment, Integer> {
    Iterable<UniversityDepartment> findAllByUniversityId(int universityId);
}
