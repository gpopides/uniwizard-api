package api.uniwizard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.CrossOrigin;

@SpringBootApplication
public class UniwizardApplication {

    public static void main(String[] args) {
        SpringApplication.run(UniwizardApplication.class, args);
    }

}
